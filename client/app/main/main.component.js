import angular from 'angular';
import uiRouter from 'angular-ui-router';
import routing from './main.routes';

export class MainController {
  $http;
  socket;
  awesomeThings = [];
  newThing = {};
  isLoggedIn: Function;
  getCurrentUser: Function;

  /*@ngInject*/
  constructor($http, $scope, socket, Auth) {
    'ngInject';

    this.$http = $http;
    this.socket = socket;
    this.isLoggedIn = Auth.isLoggedInSync;
    this.getCurrentUser = Auth.getCurrentUserSync;

    $scope.$on('$destroy', function() {
      socket.unsyncUpdates('thing');
    });
  }

  $onInit() {
    this.$http.get('/api/things')
      .then(response => {
        this.awesomeThings = response.data;
        this.socket.syncUpdates('thing', this.awesomeThings);
      });
  }

  addThing() {
    if((this.newThing.name || this.newThing.text) && this.newThing.category) {
      this.$http.post('/api/things', {
        name: this.newThing.name,
        text: this.newThing.text,
        category: this.newThing.category.trim()
      });
      this.newThing.name = '';
      this.newThing.text = '';
      this.newThing.category = '';
    }
  }

  deleteThing(thing) {
    this.$http.delete(`/api/things/${thing._id}`);
  }
}

export default angular.module('newsApp.main', [uiRouter])
  .config(routing)
  .component('main', {
    template: require('./main.html'),
    controller: MainController
  })
  .name;
