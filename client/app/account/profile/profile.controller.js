'use strict';

export default class ProfileController {

  isLoggedIn: Function;
  getCurrentUser: Function;

  constructor(Auth) {
    'ngInject';

    this.isLoggedIn = Auth.isLoggedInSync;
    this.getCurrentUser = Auth.getCurrentUserSync;
  }

}
