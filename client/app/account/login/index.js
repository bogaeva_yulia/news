'use strict';

import angular from 'angular';
import LoginController from './login.controller';

export default angular.module('newsApp.login', [])
  .controller('LoginController', LoginController)
  .name;
